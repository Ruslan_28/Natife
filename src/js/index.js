'use strict';

window.jQuery = window.$ = require('jquery');
require('slick-carousel');
require('jquery-scrollify');
require('jquery-parallax.js');

const howWeWorkSlideNames = ['Your Idea', 'Planning', 'UX design', 'UI design', 'Server side', 'App dev', 'Testing', 'Release'];

jQuery(document).ready(function ($) {

    var $slickEl = $('.center');

    $slickEl.on('init', function(event, slick) { 
        $('.slick-center .cl').removeClass('no-active');
        $('.slick-center .opacity-fonts').removeClass('no-active');
        $('.slick-center').prev('.slick-slide').find('.cl').css('justify-content', 'flex-end');
        $('.slick-center').next('.slick-slide').find('.cl').css('justify-content', 'flex-start');
    })

    $slickEl.slick({
      centerMode: true,
      centerPadding: '300px',
      slidesToShow: 1,
      focusOnSelect: false,
      dots: false,
      infinite: true,
      draggable: false,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      responsive: [
        {
          breakpoint: 1000,
          settings: {
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            draggable: true,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
    })

    $slickEl.on('afterChange', function(event, slick, currentSlide) {
        $('.slick-center .cl').removeClass('no-active');
        $('.slick-center .opacity-fonts').removeClass('no-active');
        $('.slick-center').prev('.slick-slide').find('.cl').css('justify-content', 'flex-end');
        $('.slick-center').next('.slick-slide').find('.cl').css('justify-content', 'flex-start');
        // $('.slick-center .cl').css('justify-content', 'center');
    })

    $slickEl.on('beforeChange', function(event, slick, currentSlide) {
        $('.slick-center .cl').addClass('no-active');
        $('.slick-center .opacity-fonts').addClass('no-active');
    })

    //iframe

    // const links  = document.body.querySelectorAll('.links')
    // const body = document.body;

    // links.forEach(function(item) {
    //     item.onclick = function() {
    //     const frameLink = this.dataset.frameAddress;
    
    //     const newFrame = document.createElement('iframe');
    //     newFrame.src = `./${frameLink}.html`;
    //     newFrame.classList.add('frame');

    //     $(item).addClass("transform");
    
    //     setTimeout(function() {
    //         body.innerHTML = '';
    //         body.style.overflow = 'hidden';
    //         body.appendChild(newFrame);

    //         setTimeout(function() {
    //             newFrame.style.opacity = '1'
    //         }, 0)
    //     }, 500)

    //     }
    // })

    




    
    $('#what-we-do-slider').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 768,
                settings: "unslick"
            }
        ]
    })

    $('#how-we-work-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        dots: true,
        dotsClass: 'custom-dots',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    dots: false
                },
            }
        ],
        customPaging: function (slider, i) {
            const slideNumber = (i + 1),
                totalSlides = slider.slideCount;
            return '<svg xmlns="http://www.w3.org/2000/svg" class="dot" viewBox="0 0 98.19 34.26" role="button"><defs><style>.cls-1{fill:#fff;}</style>' +
                '</defs><title></title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1">' +
                '<polygon class="cls-1" points="0.79 33.76 8.71 17 0.79 0.5 89.72 0.5 97.64 17 89.72 33.76 0.79 33.76"/>' +
                '<text x="50%" y="50%" class="dot-text" alignment-baseline="middle" text-anchor="middle">' + howWeWorkSlideNames[i] + '</text>' +
                '<path d="M89.41,1l7.68,16L89.4,33.26H1.58L9.06,17.42l.2-.43-.21-.43L1.59,1H89.41M90,0H0L8.16,17,0,34.26H90L98.19,17,90,0Z"/></g></g></svg>';
        }
    });

    $('#how-we-work-slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $('.bulb-image').toggleClass('animated');
    });

    $(".bulb-image")
        .on("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd",
            function (e) {
                $(".bulb-image").removeClass('animated');
            });

    //if true default browser scroll dont work
    let disableScroll = false;

    $(function () {
        //fixes for window scrolling
        var needScroll = false;
        if(getViewport()[0] <= 768){
            var needScroll = true;
        }
        // end fixes for scrolling
        $.scrollify({
            section: ".section",
            scrollbars: needScroll,
            overflowScroll: true,
            setHeights: false,
            scrollSpeed: 1200,
            before: function(){
                var getTopSection = $('#top-section.section.top-section');

                if($.scrollify.current()[0] === getTopSection[0]){
                    $.scrollify.disable();
                    setTimeout(() => {
                        $.scrollify.enable();
                    }, 200)
                }else{
                $.scrollify.disable();
                setTimeout(() => {
                    $.scrollify.enable();
                }, 950)
            }
            },
            afterResize: function () {
                $.scrollify.update();
            },
            after: function () {
                var a = $('footer.section.footer');

                if ($.scrollify.current()[0] === a[0]) {
                    $('.fixed-section .bottom').css('color', '#fff');
                } else {
                    $('.fixed-section .bottom').css('color', '#000');
                }

                var b = $('section.section.what-we-do');

               if ($.scrollify.current()[0] === b[0]) {
                   $.scrollify.disable();


                   $('#what-we-do-slider').on("wheel", function (event) {
                    event.preventDefault();
                       if(disableScroll) {
                           event.preventDefault();
                           return;
                       }

                       let indexSlide = $('#what-we-do-slider').slick('slickCurrentSlide');
                       
                       if(indexSlide === 0) {
                           if (event.originalEvent.deltaY > 0) {
                               $(this).slick("slickNext");
                               preventScroll();
                           } else if (event.originalEvent.deltaY < 0) {
                               $.scrollify.enable();
                               
                           }
                       }
                       
                       else if (indexSlide === 1) {

                           if (event.originalEvent.deltaY > 0) {
                               $.scrollify.enable();
                             
                           } else if (event.originalEvent.deltaY < 0) {
                               $(this).slick("slickPrev");
                               preventScroll();
                           }
                       }

                       function preventScroll() {
                           disableScroll = true;
                           setTimeout(()=> {
                               disableScroll = false;
                           }, 950)
                       }
                   })
               }
            }
        });
    });


    // $('.bg-2').parallax({imageSrc: 'assets/css/images/png/bg-image-2.png'});
    // $('.top-section').parallax({imageSrc: 'assets/css/images/png/bg-image-1.png'});
    
    $('#burger').click(function () {
        $('#burger').toggleClass('is-active');
        $('#burger').toggleClass('fixed');
        $('#fixed-menu').toggle();
    });

    // POLYGON COLORING

    $('.poly').hover(function () {
        $(this).closest('g').css('fill-opacity', '1')
    }, function () {
        $(this).closest('g').css('fill-opacity', '0')
    });

    $("#Layer_21").mousedown(function () {
        $(".group").css('fill-opacity', '1');
    });

    $("#Layer_21").mouseup(function () {
        $(".group").css('fill-opacity', '0');
    });

    $("#Layer_22").mousedown(function () {
        $(".group").css('fill-opacity', '1');
    });

    $("#Layer_22").mouseup(function () {
        $(".group").css('fill-opacity', '0');
    });

});

let handleTriangles = true;

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {

        e.preventDefault();
        $('#fixed-menu').hide();
        $('#burger').removeClass('is-active');
        $('#burger').removeClass('fixed');
        $.scrollify.instantMove(this.getAttribute('href'));
        // document.querySelector(this.getAttribute('href')).scrollIntoView({
        //     block: "start",
        //     behavior: 'smooth'
        // });
    });
});

$(window).on('resize orientationChange', function (event) {
    if (!($('#what-we-do-slider').hasClass('slick-initialized'))) {
        if (getViewport()[0] >= 768) {
            $('#what-we-do-slider').slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: "unslick"
                    }
                ]
            });
        }
    }
});

window.addEventListener('load', function () {
    const docHeight = document.documentElement.offsetHeight;
    const infoBox = document.getElementById('triangles-image-info');
    const box = document.getElementById('triangles-image');
    var stopped = false;

    box.style.display = 'block';
    infoBox.style.display = 'block';

    const mq = window.matchMedia("(min-width: 769px)");
    mq.addListener(handleTopTriangles);

    mediaCheck(mq);

    window.addEventListener('scroll', function () {

        if (handleTriangles) {
            trianglesCheck();
        }

    }, false);

    function handleTopTriangles(mq) {
        handleTriangles = mq.matches;
        if (!mq.matches) {
            box.style.display = 'none';
            infoBox.style.display = 'none';
            stopped = true;
        } else {
            box.style.display = 'none';
            infoBox.style.display = 'block';
            trianglesCheck();
        }
    }

    function trianglesCheck() {
        if (!stopped) {
            if (!elementOut()) {
                stopped = true;
            }
        } else {
            if (!elementIn()) {
                stopped = false;
            }
        }
    }

    function mediaCheck(mq) {
        handleTriangles = mq.matches;
        if (!mq.matches) {
            box.style.display = 'none';
            infoBox.style.display = 'none';
            stopped = true;
        } else {
            box.style.display = 'block';
            infoBox.style.display = 'block';
            trianglesCheck();
        }
    }

    function elementOut() {
        const bodyRect = document.body.getBoundingClientRect(),
            elemRect = document.getElementById('triangles-image').getBoundingClientRect(),
            offset = elemRect.top - bodyRect.top;

        const topSection = document.getElementById('top-section');
        const topSectionHeight = topSection.clientHeight;
        const speedMultiplier = 7250 / getViewport()[1];

        if (offset >= (topSectionHeight + ((topSectionHeight / 100) * 2))) {
            box.style.display = 'none';
            infoBox.style.display = 'block';
            return false;
        }

        const minScale = 0.2;
        const scrolled = (1 - (window.scrollY / (docHeight - window.innerHeight)) * speedMultiplier),
            transformSizeValue = 'scale(' + scrolled + ')';

        if (scrolled >= minScale) {
            box.style.WebkitTransform = transformSizeValue;
            box.style.MozTransform = transformSizeValue;
            box.style.OTransform = transformSizeValue;
            box.style.msTransform = transformSizeValue;
            box.style.transform = transformSizeValue;
        }

        return true;
    }

    function elementIn() {
        const elemRect = document.getElementById('triangles-image-info').getBoundingClientRect();
        const viewPortHeight = getViewport()[1];

        if (infoBox.style.display === "none") {
            return false;
        }

        if (elemRect.top > (viewPortHeight / 2)) {
            box.style.display = 'block';
            infoBox.style.display = 'none';
            return false;
        }

        return true;
    }

}, false);

function getViewport() {

    var viewPortWidth;
    var viewPortHeight;

    // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
    if (typeof window.innerWidth != 'undefined') {
        viewPortWidth = window.innerWidth,
            viewPortHeight = window.innerHeight
    }

    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
    else if (typeof document.documentElement != 'undefined'
        && typeof document.documentElement.clientWidth !=
        'undefined' && document.documentElement.clientWidth != 0) {
        viewPortWidth = document.documentElement.clientWidth,
            viewPortHeight = document.documentElement.clientHeight
    }

    // older versions of IE
    else {
        viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
            viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
    }
    return [viewPortWidth, viewPortHeight];
}

//filter-color image
$(".about_image").hover(function () {
    $(this).toggleClass('filter');
});


/** use jquery because scrollify reads the position of the elements in the DOM tree 
 * and when using css the position of the elements changes only visually, 
 * and the scroll breaks down
 * */
if(getViewport()[0] <= 768){
    $('.info-section').insertAfter($('.what-we-do'));
}

$(document).ready(function(){ 
    var body = $("body"); 
    body.fadeIn(400); 
    $(document).on("click", "a:not([href^='#']):not([href^='tel']):not([href^='mailto'])", function(e) { 
        // debugger
        e.preventDefault(); 
        $("body").fadeOut(400); 
        
        var self = this;
        setTimeout(function () { 
        window.location.href = $(self).attr("href");
        }, 400); 
    });

    
    $('#click-top').click(function() {
        $('#triangles-image').css('transform', 'scale(1)');
        $('#what-we-do-slider').slick("slickPrev");
    })

    

  });