'use strict';

const JS_ENTRY = "./src/js/index.js",
    SCSS_ENTRY = "./src/scss/styles.scss",
    SRC_HTML = "./src/*.html",
    SRC_CSS = "./src/scss/**/*.scss",
    SRC_JS = "./src/js/**/*.js";

const DEST = "public",
    DEST_CSS = "./public/assets/css",
    DEST_FONTS = "./public/assets/fonts",
    DEST_JS = "./public/assets/js";

const gulp = require('gulp'),
    critical = require('critical').stream,
    sass = require('gulp-sass'),
    sassGlob = require('gulp-sass-glob'),
    autoprefixer = require('gulp-autoprefixer'),
    htmlmin = require('gulp-htmlmin'),
    cleanCSS = require('gulp-clean-css'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    uglify = require('gulp-uglify'),
    buffer = require("vinyl-buffer"),
    source = require("vinyl-source-stream");

gulp.task('build', function() {
    gulp.run('build:css:minify');
    gulp.run('build:js:minify');
    gulp.run('build:pages:minify');
});

gulp.task('build:css:minify', function () {
    return gulp.src([SCSS_ENTRY])
        .pipe(sassGlob())
        .pipe(sass().on('error', swallowError))
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8', level: {1: {specialComments: 0}}}))
        .pipe(gulp.dest(DEST_CSS));
});

gulp.task('build:js:minify', function () {

    return browserify({
        debug: true,
        extensions: ['.js']
    }).transform(babelify.configure({presets: ["es2015", "stage-0"],  sourceMapRelative: "$PWD/src/js"}))
        .require(JS_ENTRY, { entry: true })
        .bundle()
        .on('error', swallowError)
        .pipe(source("index.js"))
        .pipe(buffer())
        // .pipe(uglify())
        .pipe(gulp.dest(DEST_JS));
});

gulp.task('build:pages:minify', function() {
    return gulp.src([SRC_HTML])
        .pipe(critical({base: 'public/', inline: true, css: [DEST_CSS + '/styles.css'], include: [] }))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: false
        }))
        .pipe(gulp.dest(DEST));
});

function swallowError (error) {
    console.error(error.toString());
    this.emit('end');
}